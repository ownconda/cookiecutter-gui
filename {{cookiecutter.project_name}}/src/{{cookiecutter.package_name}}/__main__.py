import sys

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication
import click

# import own_commongui

from . import util
from .gui.mainwindow import MainWindow



#
# This is actually in our commongui lib
#
def dark_mode_enabled():
    """Return ``True`` if the user uses dark dark desktop theme."""
    p = QApplication.palette()
    return p.window().color().lightness() < 128


def setup(display, appname=None, icon=None, project=None, dist_name=None,
          org_name='own', org_domain='owncompany.com'):
    """Setup and return the `QApplication`.

    :param display:    The display name
    :param appname:    The application name (used, e.g., for settings),
                       defaults to lower case *display* name
    :param icon:       The window icon ``QIcon`` or icon theme name,
                       defaults to *appname*
    :param project:    The app's gitlab project (used by the except hook),
                       defaults to "training/*appname*"
    :param dist_name:  Override distribution name (generated from org and
                       project basename)
    :param org_name:   The organization name (used, e.g., for settings)
    :param org_domain: The organization domain (used, e.g., for settings)
    :returns: ``QApplication`` instance

    """
    # own_breeze_icons.setup()  # Set custom icon theme as default

    if appname is None:
        appname = display.lower()

    if project is None:
        project = f'apps/{appname}'

    if icon is None:
        icon = appname
    if isinstance(icon, str):
        icon = QIcon.fromTheme(icon)

    if not dist_name:
        pname = project.split('/')[-1]
        dist_name = f'{org_name}-{pname}'

    app = QApplication(list(sys.argv))
    app.setApplicationDisplayName(display)
    app.setApplicationName(appname)
    app.setApplicationVersion(_get_version(dist_name))
    app.setOrganizationDomain(org_domain)
    app.setOrganizationName(org_name)
    app.setWindowIcon(icon)

    app.dist_name = dist_name
    app.project = project

    if dark_mode_enabled():
        QIcon.setThemeName('ownicons-dark')
    else:
        QIcon.setThemeName('ownicons')
    return app


def _get_version(dist):
    # Check if project is installed "editable" (pip install -e .)
    project_dir = None
    for path_item in sys.path:
        path = pathlib.Path(path_item, f'{dist}.egg-link')
        if path.is_file():
            project_dir = path.open().readline().strip()
            break

    if project_dir:
        # Editable install
        return _dev_version(project_dir)
    else:
        return pkg_resources.get_distribution(dist).version


def _dev_version(path):
    version = subprocess.check_output(
        ['git', 'describe', '--tags', '--long', 'HEAD'], cwd=path
    ).decode().strip().replace('-', '.', 1)
    return version
#
# end lib
#


@click.command()
def main():
    """Start {{ cookiecutter.project_title }}."""
    # app = own_commongui.setup('{{ cookiecutter.project_title }}', icon=QIcon(util.get_icon('{{ cookiecutter.project_name }}')))
    app = setup('{{ cookiecutter.project_title }}', icon=QIcon(util.get_icon('{{ cookiecutter.project_name }}')))
    w = MainWindow()
    w.showMaximized()
    sys.exit(app.exec())


if __name__ == "__main__":
    main()  # pylint: disable=no-value-for-parameter
