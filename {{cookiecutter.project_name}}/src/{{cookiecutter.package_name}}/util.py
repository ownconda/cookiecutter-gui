import pathlib


__all__ = ['get_icon', 'get_resource']


RESOURCE_DIR = pathlib.Path(__file__).parent / 'resources'


def get_icon(name):
    """Return the path to the icon named *name*."""
    return str(RESOURCE_DIR / f'{name}.svg')


def get_resource(filename):
    """Return the full path to the resource file *filename*."""
    return str(RESOURCE_DIR / filename)
