from PyQt5.QtWidgets import QWidget


class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        # This window title is suffixed with the application name automatically.
        # The "[*]" is a place holder for the "document modified" marker.
        self.setWindowTitle('Document Name [*]')  #
        # self.setWindowModified(True or False)
