{%- set title = "Welcome to {}'s documentation!".format(cookiecutter.project_title) -%}
{{ '=' * title|length }}
{{ title }}
{{ '=' * title|length }}

Contents:

.. toctree::
   :maxdepth: 2


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
