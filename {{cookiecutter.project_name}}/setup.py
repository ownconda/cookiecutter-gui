import os

from setuptools import setup, find_packages


#
# NOTE: Package meta data has to be sync'ed manually with conda/meta.yaml!
#
setup(
    name='{{ cookiecutter.conda_name }}',
    version=f'{os.getenv("GIT_DESCRIBE_TAG", 0)}.{os.getenv("GIT_DESCRIBE_NUMBER", 0)}',
    description='{{ cookiecutter.project_summary.replace("'", "\\'") }}',
    url='https://gitlab.com/{{ cookiecutter.gitlab_group }}/{{ cookiecutter.project_name }}',
    install_requires=[],
    packages=find_packages(where='src'),
    package_dir={'': 'src'},
    include_package_data=True,
    entry_points={
{%- if cookiecutter.has_click != 'n' %}
        'console_scripts': [
            '{{ cookiecutter.project_name }}={{ cookiecutter.package_name }}.__main__:main',
        ],
{%- else %}
        'console_scripts': [],
{%- endif %}
    },
    classifiers=[
        'Private :: Do Not Upload',
    ],
)
