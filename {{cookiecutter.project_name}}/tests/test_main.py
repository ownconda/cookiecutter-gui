import click.testing
import pytest

from {{ cookiecutter.package_name }}.__main__ import main


@pytest.fixture
def runner():
    return click.testing.CliRunner()


def test_main(runner):
    result = runner.invoke(main)
    assert result.exit_code == 0
    assert result.output.startswith('Replace this message')
