import pytest

from {{ cookiecutter.package_name }} import model


@pytest.fixture(autouse=True)
def db():  # pylint: disable=invalid-name
    engine = model.get_engine()
    model.METADATA.drop_all(engine)
    model.METADATA.create_all(engine)


def test_model():
    assert False
