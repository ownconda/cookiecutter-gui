{{ cookiecutter.project_title }}
{{ '=' * cookiecutter.project_title|length }}

[![pipeline status](https://gitlab.com/{{ cookiecutter.gitlab_group }}/{{ cookiecutter.project_name }}/badges/master/build.svg)](https://gitlab.com/{{ cookiecutter.gitlab_group }}/{{ cookiecutter.project_name }}/commits/master)
[![coverage report](https://gitlab.com/{{ cookiecutter.gitlab_group }}/{{ cookiecutter.project_name }}/badges/master/coverage.svg)](https://gitlab.com/{{ cookiecutter.gitlab_group }}/{{ cookiecutter.project_name }}/commits/master)

{{ cookiecutter.project_summary }}
{%- if cookiecutter.has_docs != 'n' %}


Documentation
-------------

You can find the documentation at
https://forge.services.own/docs/{{ cookiecutter.gitlab_group }}/{{ cookiecutter.project_name }}
{%- endif %}
